USE [ApplicationConfiguration]
GO
/****** Object:  StoredProcedure [dbo].[Customer_SalesData]    Script Date: 7/31/2019 4:02:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROC [dbo].[Customer_SalesData]
@FirstName varchar(100),
@LastName varchar(100),
@PhoneNumber int,
@Product varchar(100)
As
Begin
DECLARE @Cnt1 int
DECLARE @Cnt int
DECLARE @product1 int
set @Cnt1=(select customerId from Customer where PhoneNumber=@PhoneNumber)
if(@Cnt1!=null)
begin
set @product1= (select ProductId from CustomerOrder where CustomerId=@Cnt1)
if(@product1!=null)
begin 
update CustomerOrder set Score=Score+1 where ProductId=@product1
end
else
begin 
insert into CustomerOrder values(@Cnt1,@product1,1)
end 
end
else
begin
insert into Customer values (1, @FirstName, @LastName, @PhoneNumber)
set @Cnt1=(select customerId from Customer where PhoneNumber=@PhoneNumber)
set @product1 = (select ProductId from Product where Product=@Product)
insert into CustomerOrder values (@Cnt1, @product1, 1)
end
end

GO
/****** Object:  StoredProcedure [dbo].[Inventory_Insertion]    Script Date: 7/31/2019 4:02:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROC [dbo].[Inventory_Insertion]
@Product varchar(100),
@ProductCategory varchar(100),
@InventoryCount int
As
Begin
DECLARE @Cnt int
set @Cnt = (select ProductId from Product where Product=@Product)
if(@Cnt!=null)
begin
update Inventory set InventoryCount=@InventoryCount where ProductId=@Cnt
end
Else
Begin
insert into Product values(1, @ProductCategory, @Product, 0)
insert into Inventory values(1,@InventoryCount);
End
end
GO
/****** Object:  StoredProcedure [dbo].[Inventory_Suggestion]    Script Date: 7/31/2019 4:02:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROC [dbo].[Inventory_Suggestion]
@Date int
As
Begin
Declare @product int
create table tmp1(productId int, score int)
insert into tmp1 select ProductId, score from CustomerOrder where DateOfTran=@Date order by score desc
set @product= (select top 3 productId from tmp1)
select productId from Inventory where ProductId=@product
end
GO
/****** Object:  StoredProcedure [dbo].[Specific_Promotion]    Script Date: 7/31/2019 4:02:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROC [dbo].[Specific_Promotion]
@PhoneNumber int
As
Begin
DECLARE @CustId int
DECLARE @ProductId int
DECLARE @product int
DECLARE @Productcategory int
set @CustId = (select customerId from Customer where PhoneNumber=@PhoneNumber)
create table tmp(productId int, score int)
insert into tmp select ProductId, score from CustomerOrder where CustomerId=@CustId order by score desc
set @ProductId=(select ProductId from tmp)
set @product=(select product from Product where ProductId=@ProductId)
set @Productcategory=(select ProductCategory from Product where ProductId=@ProductId)
select Promotion from Promotion where ProductCategory=@Productcategory or Product=@product
end
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 7/31/2019 4:02:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customer](
	[customerId] [int] NOT NULL,
	[FirstName] [varchar](100) NULL,
	[LastName] [varchar](100) NULL,
	[PhoneNumber] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[customerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CustomerOrder]    Script Date: 7/31/2019 4:02:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerOrder](
	[CustomerId] [int] NULL,
	[ProductId] [int] NULL,
	[Score] [int] NULL,
	[DateOfTran] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Inventory]    Script Date: 7/31/2019 4:02:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inventory](
	[ProductId] [int] NULL,
	[InventoryCount] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Product]    Script Date: 7/31/2019 4:02:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Product](
	[ProductId] [int] NOT NULL,
	[ProductCategory] [varchar](100) NULL,
	[Product] [varchar](100) NULL,
	[SalesCount] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Promotion]    Script Date: 7/31/2019 4:02:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Promotion](
	[Product] [varchar](100) NULL,
	[ProductCategory] [varchar](100) NULL,
	[promotion] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tmp]    Script Date: 7/31/2019 4:02:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tmp](
	[productId] [int] NULL,
	[score] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tmp1]    Script Date: 7/31/2019 4:02:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tmp1](
	[productId] [int] NULL,
	[score] [int] NULL
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[CustomerOrder]  WITH CHECK ADD  CONSTRAINT [fk_CId] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([customerId])
GO
ALTER TABLE [dbo].[CustomerOrder] CHECK CONSTRAINT [fk_CId]
GO
ALTER TABLE [dbo].[CustomerOrder]  WITH CHECK ADD  CONSTRAINT [fk_PId] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([ProductId])
GO
ALTER TABLE [dbo].[CustomerOrder] CHECK CONSTRAINT [fk_PId]
GO
ALTER TABLE [dbo].[Inventory]  WITH CHECK ADD  CONSTRAINT [fk_Id] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([ProductId])
GO
ALTER TABLE [dbo].[Inventory] CHECK CONSTRAINT [fk_Id]
GO
